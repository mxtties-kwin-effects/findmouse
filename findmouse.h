#ifndef KWIN_TRACKMOUSE_H
#define KWIN_TRACKMOUSE_H

#include <kwineffects.h>
#include <kwinglutils.h>

namespace KWin
{

class FindMouseEffect
    : public Effect
    {
    public:
        FindMouseEffect();
        virtual ~FindMouseEffect();
        virtual void reconfigure( ReconfigureFlags );
        virtual void prePaintScreen( ScreenPrePaintData& data, int time );
        virtual void paintScreen( int mask, QRegion region, ScreenPaintData& data );
        virtual void postPaintScreen();
        virtual void mouseChanged( const QPoint& pos, const QPoint& old,
            Qt::MouseButtons buttons, Qt::MouseButtons oldbuttons,
            Qt::KeyboardModifiers modifiers, Qt::KeyboardModifiers oldmodifiers );

    private:
        QRect starRect( int num ) const;
        void loadTexture();
        bool active;
        int angle;
        GLTexture* texture;
        QSize textureSize;
        TimeLine timeLine;
    };

} // namespace

#endif
