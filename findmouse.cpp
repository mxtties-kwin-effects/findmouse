/********************************************************************
inspired by trackmouse effect
*********************************************************************/

#include "findmouse.h"

#include <QTime>

#include <kwinconfig.h>

#include <kglobal.h>
#include <kstandarddirs.h>

#include <math.h>

#ifdef KWIN_HAVE_OPENGL_COMPOSITING
#include <GL/gl.h>
#endif

#include <kdebug.h>

namespace KWin
{

KWIN_EFFECT( findmouse, FindMouseEffect )

const int DIST = 50;

FindMouseEffect::FindMouseEffect()
    : active( false )
    , angle( 0 )
    , texture( NULL )
    {
    reconfigure(ReconfigureAll);
    effects->startMousePolling(); // We require it to detect activation as well
    }

FindMouseEffect::~FindMouseEffect()
    {
    effects->stopMousePolling();
    delete texture;
    }

void FindMouseEffect::reconfigure(ReconfigureFlags flags)
{
    timeLine.setDuration(2000);
}

void FindMouseEffect::prePaintScreen( ScreenPrePaintData& data, int time )
    {
    if( active ) {
        timeLine.addTime(time);
    }
    effects->prePaintScreen( data, time );
    }

void FindMouseEffect::paintScreen( int mask, QRegion region, ScreenPaintData& data )
    {
    effects->paintScreen( mask, region, data ); // paint normal screen
    if( !active )
        return;
#ifdef KWIN_HAVE_OPENGL_COMPOSITING
    if( texture )
        {
        glPushAttrib( GL_CURRENT_BIT | GL_ENABLE_BIT );
        texture->bind();
        QRect mergedSize;
        for( int screen = 0; screen < effects->numScreens(); screen++ )
            {
            QRect screenGeom = effects->clientArea( ScreenArea, screen, 0 );
            mergedSize = mergedSize.united(screenGeom);
            }
        int w = mergedSize.width();
        int h = mergedSize.height();


        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glViewport(0,0,w,h);
        glLoadIdentity();

        glOrtho(0.0f,w,h,0.0f,-1.0f,1.0f);

        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glLoadIdentity();
        texture->bind();
        float qx = qMax(cursorPos().x(), w-cursorPos().x());
        float qy = qMax(cursorPos().y(), h-cursorPos().y());
        float size = sqrt(pow(qx, 2) + pow(qy, 2));
    //    qDebug() << "size = " << size;
        glTranslatef(cursorPos().x(),cursorPos().y(), 0);
        glBegin(GL_QUADS);
        glColor3f(0,0,0);
        float focusSize = 50.f;

        float s = (size / focusSize) - .5f;

        float t = timeLine.progress();
        glTexCoord2f(-t*s,-t*s);
        glVertex2f(-size,-size);
        glTexCoord2f(1.f+t*s,-t*s);
        glVertex2f(size,-size);
        glTexCoord2f(1.f+t*s,1.f+t*s);
        glVertex2f(size,size);
        glTexCoord2f(-t*s,1.f+t*s);
        glVertex2f(-size,size);
        glEnd();

        texture->unbind();
        glPopAttrib();
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        }
#endif
    }

void FindMouseEffect::postPaintScreen()
    {
    if( active )
        {
            effects->addRepaintFull();
        }
    effects->postPaintScreen();
    }

void FindMouseEffect::mouseChanged( const QPoint& p1, const QPoint& p2, Qt::MouseButtons,
    Qt::MouseButtons, Qt::KeyboardModifiers modifiers, Qt::KeyboardModifiers )
    {
    if( modifiers == ( Qt::CTRL | Qt::META ))
        {
        if( !active )
            {
            if( texture == NULL )
                loadTexture();
            if( texture == NULL )
                return;
            active = true;
            angle = 0;
            }
        }
    else
        {
        if( active )
            {
                timeLine.setProgress(0);
            active = false;
            }
        }
    }

void FindMouseEffect::loadTexture()
    {
#ifdef KWIN_HAVE_OPENGL_COMPOSITING
    QString file = KGlobal::dirs()->findResource( "appdata", "findmouse.png" );
    if( file.isEmpty())
        return;
    QImage im( file );
    texture = new GLTexture( im );
    textureSize = im.size();
#endif
    }

} // namespace
