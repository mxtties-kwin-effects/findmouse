#ifndef KWIN_TRACKMOUSE_CONFIG_H
#define KWIN_TRACKMOUSE_CONFIG_H

#include <kcmodule.h>


namespace KWin
{

class FindMouseEffectConfig : public KCModule
    {
    Q_OBJECT
    public:
        explicit FindMouseEffectConfig(QWidget* parent = 0, const QVariantList& args = QVariantList());
        ~FindMouseEffectConfig();

    public slots:
        virtual void save();
        virtual void load();
        virtual void defaults();
    };

} // namespace

#endif
