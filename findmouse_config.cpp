#include "findmouse_config.h"

#include <kwineffects.h>

#include <klocale.h>
#include <kdebug.h>
#include <kaction.h>
#include <KShortcutsEditor>

#include <QVBoxLayout>
#include <QLabel>

namespace KWin
{

KWIN_EFFECT_CONFIG_FACTORY

FindMouseEffectConfig::FindMouseEffectConfig(QWidget* parent, const QVariantList& args) :
        KCModule(EffectFactory::componentData(), parent, args)
    {
    QVBoxLayout* layout = new QVBoxLayout(this);
    QLabel* label = new QLabel(i18n("Hold Ctrl+Meta keys to see where the mouse cursor is."), this);
    label->setWordWrap(true);
    layout->addWidget(label);

    layout->addStretch();

    load();
    }

FindMouseEffectConfig::~FindMouseEffectConfig()
    {
    }

void FindMouseEffectConfig::load()
    {
    KCModule::load();

    emit changed(false);
    }

void FindMouseEffectConfig::save()
    {
    KCModule::save();

    emit changed(false);
    EffectsHandler::sendReloadMessage( "findmouse" );
    }

void FindMouseEffectConfig::defaults()
    {
    emit changed(true);
    }


} // namespace

#include "findmouse_config.moc"
